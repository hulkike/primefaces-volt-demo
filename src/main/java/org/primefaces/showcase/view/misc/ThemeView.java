/*
 * Copyright 2009-2015 PrimeTek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.primefaces.showcase.view.misc;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class ThemeView implements Serializable {

    /** 默认主题：volt */
    private String theme = "volt";
    /** 布局的CSS样式文件 */
    private String layoutCss = "css/volt-layout.css";
    private Map<String, String> layoutMap;

    @PostConstruct
    public void init() {
        layoutMap = new HashMap<String, String>();
        layoutMap.put("volt", "css/volt-layout.css");
//        layoutMap.put("volt-blue", "css/volt-layout-blue.css");
//        layoutMap.put("volt-orange", "css/volt-layout-orange.css");
//        layoutMap.put("volt-red", "css/volt-layout-red.css");
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
        // 根据主题的颜色变化，重置页面布局CSS
        layoutCss = layoutMap.get(theme);
        if (layoutCss == null) {
            layoutCss = "css/volt-layout.css";
        }
    }

    public String getLayoutCss() {
        return layoutCss;
    }

    public void setLayoutCss(String layoutCss) {
        this.layoutCss = layoutCss;
    }
}
